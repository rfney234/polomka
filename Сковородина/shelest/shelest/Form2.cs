﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace shelest
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            ///вывод данных в таблицу
            this.agentTableAdapter.Fill(this.user1DataSet.Agent);

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var f1 = new Form1();
            f1.Show();
            this.Hide();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var f3 = new Form3();
            f3.Show();
            this.Hide();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Точно удалить агента?");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Данные сохранены");
        }

        private void button7_Click(object sender, EventArgs e)
        {
            var f4 = new Form4();
            f4.Show();
            this.Hide();
        }
    }
}
