﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace polomka
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            textBox2.UseSystemPasswordChar = true;
            checkBox1.Text = "Показать пароль";
        }
        SqlConnection connection = new SqlConnection("server = localhost;database = polomka;integrated security = true;");/// <summary>
                                                                                                                          /// Подключение бд
                                                                                                                          /// </summary>
                                                                                                                          /// <param name="sender"></param>
                                                                                                                          /// <param name="e"></param>
                                                                                                                          /// 

        int admin = 0;
        private void button2_Click(object sender, EventArgs e)
        {
            Hide();
        }
        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == '0')
            {
                admin++;
            }
            if(admin == 4)
            {
                MessageBox.Show("Привет Администратор");
                var admin = new admin();
                admin.Show();
                Hide();
            }
            
        }
        private void button1_Click(object sender, EventArgs e)///проверка авторизациии
        {
           
            try
            {
                int error = 0, Role = 0;
                for (int plus = 0; plus < 1; plus++)
                {
                    if (textBox1.Text == "" && textBox2.Text == "")
                    {
                        error = 1;
                        MessageBox.Show("Какое то из полей пустое ", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        break;
                    }
                    if (textBox1.Text != "" && textBox2.Text != "")
                    {
                        SqlCommand command = new SqlCommand("select * from Role where Login = @l and Password = @pas and Role=0", connection);
                        connection.Open();
                        command.Parameters.AddWithValue("@l", textBox1.Text);
                        command.Parameters.AddWithValue("@pas", textBox2.Text);
                        SqlDataReader DR = command.ExecuteReader();
                        if (DR.HasRows == true)
                        {

                            Role = 1;
                        }
                        else
                        {
                            Role = 0;
                        }
                        DR.Close();
                        connection.Close();
                    }
                    if (textBox1.Text != "" && textBox2.Text != "" && Role == 0)
                    {
                        SqlCommand command = new SqlCommand("select * from Role where Login = @l and Password = @pas and Role=1", connection);
                        connection.Open();
                        command.Parameters.AddWithValue("@l", textBox1.Text);
                        command.Parameters.AddWithValue("@pas", textBox2.Text);
                        SqlDataReader DR = command.ExecuteReader();
                        if (DR.HasRows == true)
                        {

                            Role = 2;
                        }
                        else
                        {
                            Role = 3;
                        }
                        DR.Close();
                        connection.Close();
                    }
                    if (Role == 1)
                    {
                        MessageBox.Show("Привет клиент");
                        var client = new client();
                        client.Show();
                        Hide();
                    }
                    if (Role == 0)
                    {
                        MessageBox.Show("Такого пользователя не найдено");
                        
                        textBox1.Text = "";
                        textBox2.Text = "";
                    }
                    if (Role == 2)
                    {
                        MessageBox.Show("Привет Администратор");
                        var admin = new admin();
                        admin.Show();
                        Hide();
                    }
                }

            }catch
            { MessageBox.Show("Не верные данные ", "Ошибка", MessageBoxButtons.OK,MessageBoxIcon.Error);
                textBox1.Text = "";
                textBox2.Text = "";
            }
            }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if(checkBox1.Checked == true)
            {
                checkBox1.Text = "Скрыть пароль";
                textBox2.UseSystemPasswordChar = false;
            }
            if (checkBox1.Checked == false)
            {
                checkBox1.Text = "Показать пароль";
                textBox2.UseSystemPasswordChar = true;
            }
        }

       
    }
}
