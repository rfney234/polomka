﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace polomka
{
    public partial class client : Form
    {
        public client()
        {
            InitializeComponent();
            
        }
        SqlConnection connection = new SqlConnection("server = localhost;database = polomka;integrated security = true;");/// <summary>
        /// подключение к бд
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void client_Load(object sender, EventArgs e)
        {
            Update();
        }
        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
           switch(comboBox2.Text)
            {
                case "Товары":
                    Update();
                    break;
                case "Услуги":
                    Service();
                    break;
            }
        }
        public void Service()
        {
            SqlDataAdapter s = new SqlDataAdapter("select * from Service",connection);
            DataSet seaa = new DataSet();
            s.Fill(seaa);
            dataGridView1.DataSource = seaa.Tables[0];

        }
        public void Update()
        {
            SqlDataAdapter data = new SqlDataAdapter("select Product.ID as [Код товара] , Title as [Название],Cost as [Цена],Description as [Производитель], isActive as [Можно купить],Quantity as [Количество]  from Product inner join ProductSale on Product.Id=Product.ID", connection);
            DataSet set = new DataSet();
            data.Fill(set);
            dataGridView1.DataSource = set.Tables[0];
            
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_MouseHover(object sender, EventArgs e)
        {
            textBox1.Text = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                SqlCommand delete = new SqlCommand("delete Product where Product.ID =" + textBox1.Text, connection);
                connection.Open();
                delete.ExecuteNonQuery();
                Update();
                textBox1.Text = "";
                MessageBox.Show("Поздравляю вы купили товар, с вами свяжется менеджер для уточнение деталей", "Поздравление ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch
            {
                MessageBox.Show("Ошибка данных");
                textBox1.Text = "";
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var start = new Form1();
            start.Show();
            Hide();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string select = "";
            switch(comboBox1.Text)
            {
                case "От дешевого к дорогому":
                    select = "order by Cost asc";
                    break;
                case "От дорого к дешевому":
                    select = "order by Cost desc";
                    break;
                

            }
            SqlDataAdapter data = new SqlDataAdapter("select Product.ID as [Код товара] , Title as [Название],Cost as [Цена],Description as [Производитель], isActive as [Можно купить],Quantity as [Количество]  from Product inner join ProductSale on Product.Id=Product.ID "+select, connection);
            DataSet set = new DataSet();
            data.Fill(set);
            dataGridView1.DataSource = set.Tables[0];

        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                MessageBox.Show("Ваша запись отпраленна на обработку спасибо что вы  с нами ,ваша проблема"+textBox1.Text +"мы будем ждать вас,что бы решить ее");
                textBox1.Text = "";
            }
            catch
            {
                MessageBox.Show("ошибка данных");
            }
        }

       
    }
}
