﻿using System;

namespace WSUniversalLib
{
    public class Calculation
    {
        public static int Count(int count, int productType, int width, int length, int materialType)
        {
            double cof = 0;
            double defectPercent = 0;
            double result = 0;
            switch (productType)
            {
                case 1:
                    cof = 1.1;
                    break;
                case 2:
                    cof = 2.5;
                    break;
                case 3:
                    cof = 8.43;
                    break;
                default: return -1;
            }
            switch (materialType)
            {
                case 1:
                    defectPercent = 0.3;
                    break;
                case 2:
                    defectPercent = 0.12;
                    break;
                default:return -1;
            }
            //count * width*length* cof
            double CountProduct = count * width * length * cof;
            result = (CountProduct * defectPercent) / 100;
            result  +=CountProduct;
            
            return Convert.ToInt32(Math.Ceiling(result));

        }
    }
   
}
