﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Collections;
namespace Приятный_шелест
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        string currentPath = Directory.GetCurrentDirectory();
        int CurrentPage = 0;
        int CountPage = 0;
        int CountRows = 10;
        int j = 0;
        string filter = "where ";
        ArrayList listID = new ArrayList();
        private void Form1_Load(object sender, EventArgs e)
        {
           
            Count_Page();
          
            LoadComboBox();
            LoadList();
            comboBox1.SelectedIndex = 0;
        }
        private void LoadComboBox()
        {
            using (SqlConnection connection = new SqlConnection(Auth_user.connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("Select * from AgentType order by Title desc", connection);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        listID.Add(reader[0].ToString());
                        comboBox1.Items.Add(reader[1].ToString());
                    }
                }
                connection.Close();
            }
        }
        private void Count_Page()
        {
            using (SqlConnection connection = new SqlConnection(Auth_user.connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand("Select Count(ID) from Agent",connection);
                SqlDataReader reader = command.ExecuteReader();
                reader.Read();
                MessageBox.Show(reader[0].ToString());
                CountPage = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(Convert.ToInt32(reader[0].ToString()) / CountRows)));
                connection.Close();
            }
        }
        private void LoadList()
        {
            string post ="";
            if (filter !="where ")
            {
                post = filter;
            }
            filter.Remove(filter.Length - 3);
            using (SqlConnection connection = new SqlConnection(Auth_user.connectionString))
            {
                connection.Open();
                SqlDataAdapter adapter = new SqlDataAdapter($"Select  Agent.Title as 'Наименование',(Select sum(ProductSale.ProductCount) from ProductSale where ProductSale.AgentID=Agent.ID) as 'Количество продаж', (Select sum(ProductSale.ProductCount) from ProductSale where ProductSale.AgentID=Agent.ID) as 'Размер скидки в %', Phone as 'Телефон'  FROM (Agent inner join AgentType ON AgentType.ID=Agent.AgentTypeID)  order by Agent.Title offset {CountRows *CurrentPage} rows fetch next {CountRows} rows only ", connection);
                DataSet ds = new DataSet();
                adapter.Fill(ds);
                dataGridView1.DataSource = ds.Tables[0];
                connection.Close();
                for (int i = 0; i <= dataGridView1.Rows.Count-1; i++)
                {
                    if (dataGridView1.Rows[i].Cells[2].Value.ToString() != "")
                    {
                        if ((Convert.ToInt32(dataGridView1.Rows[i].Cells[2].Value.ToString())) >= 10000)
                        {
                            if ((Convert.ToInt32(dataGridView1.Rows[i].Cells[2].Value.ToString())) >= 10000 && (Convert.ToInt32(dataGridView1.Rows[i].Cells[2].Value.ToString())) < 50000)
                            {
                                dataGridView1.Rows[i].Cells[2].Value = "5";
                            }
                            else
                         if ((Convert.ToInt32(dataGridView1.Rows[i].Cells[2].Value.ToString())) >= 50000 && (Convert.ToInt32(dataGridView1.Rows[i].Cells[2].Value.ToString())) <= 150000)
                            {
                                dataGridView1.Rows[i].Cells[2].Value = "10";
                            }
                            else
                                 if ((Convert.ToInt32(dataGridView1.Rows[i].Cells[2].Value.ToString())) >= 500000)
                            {
                                dataGridView1.Rows[i].Cells[2].Value = "25";
                            }
                        }
                        else
                        {
                            dataGridView1.Rows[i].Cells[2].Value = "0";
                        }
                    }
                    else
                    {
                        dataGridView1.Rows[i].Cells[2].Value = "0";
                    }
                    if (dataGridView1.Rows[i].Cells[1].Value.ToString() == "")
                    {
                        dataGridView1.Rows[i].Cells[1].Value = "0";
                    }
                    
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (CurrentPage < (CountPage - 1))
            {
                label1.Text = "Страница:" + (CurrentPage+1);
                CurrentPage++;
                LoadList();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (CurrentPage != 0)
            {
                label1.Text = "Страница:" + (CurrentPage + 1);
                CurrentPage--;
                LoadList();
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (comboBox1.SelectedIndex != 0)
                {
                    using (SqlConnection connection = new SqlConnection(Auth_user.connectionString))
                    {
                        connection.Open();
                        SqlCommand command = new SqlCommand($"Select  Agent.Title as 'Наименование',(Select sum(ProductSale.ProductCount) from ProductSale where ProductSale.AgentID=Agent.ID) as 'Количество продаж', (Select sum(ProductSale.ProductCount) from ProductSale where ProductSale.AgentID=Agent.ID) as 'Размер скидки в %', Phone as 'Телефон'  FROM (Agent inner join AgentType ON AgentType.ID=Agent.AgentTypeID) where Agent.AgentTypeID=", connection);
                        SqlDataReader reader = command.ExecuteReader();
                        reader.Read();
                        MessageBox.Show(reader[0].ToString());
                        CountPage = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(Convert.ToInt32(reader[0].ToString()) / CountRows)));
                        connection.Close();
                    }
                }
            }
            catch
            {
                
            }
        }
    }
}
