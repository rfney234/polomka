﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shakirov
{
    public partial class добавление : Form
    {
        public добавление()
        {
            InitializeComponent();
            MessageBox.Show("Заполните все поля!");
        }

        private void добавление_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "user3DataSet.AgentType". При необходимости она может быть перемещена или удалена.
            this.agentTypeTableAdapter.Fill(this.user3DataSet.AgentType);

        }
        /// <summary>
        /// реализация добавления агента
        /// </summary>
        private void add()
        {
            // подключение к бд
            using (SqlConnection connection = new SqlConnection(@"server=M11-00\SQLEXPRESS2021;user id=user3;password=oatkuser3;initial catalog=user3;integrated security=false"))
            {
                if (tb1.Text=="" || cb2.Text == "" || tb2.Text=="" || tb3.Text == "" || tb4.Text == "" || tb5.Text == "" || tb6.Text == "" || tb7.Text == ""
                    || tb8.Text == "" || tb9.Text == "")
                {
                    MessageBox.Show("Заполните все поля!");
                }
                else
                {
                    SqlCommand command = new SqlCommand("insert into Agent(Title,AgentTypeID,Priority,Logo,Address,INN,KPP,DirectorName,Phone,Email,Discount) values(@a1,@a2,@a3,@a4,@a5,@a6,@a7,@a8,@a9,@a10,@a11)", connection);
                    command.Parameters.AddWithValue("@a1", tb1.Text);
                    command.Parameters.AddWithValue("@a2", cb2.Text);
                    command.Parameters.AddWithValue("@a3", tb2.Text);
                    command.Parameters.AddWithValue("@a4", tb3.Text);
                    command.Parameters.AddWithValue("@a5", tb4.Text);
                    command.Parameters.AddWithValue("@a6", tb5.Text);
                    command.Parameters.AddWithValue("@a7", tb6.Text);
                    command.Parameters.AddWithValue("@a8", tb7.Text);
                    command.Parameters.AddWithValue("@a9", tb8.Text);
                    command.Parameters.AddWithValue("@a10", tb9.Text);
                    command.Parameters.AddWithValue("@a11", "15");
                    command.Connection.Open();
                    command.ExecuteNonQuery();
                    Hide();
                    var v1 = new агенты();
                    v1.Show();
                    MessageBox.Show("Готово");
                }
            
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Hide();
            var v1 = new агенты();
            v1.Show();
        }

        private void b1_Click(object sender, EventArgs e)
        {
            //вызов метода
            add();
        }

        private void tb2_TextChanged(object sender, EventArgs e)
        {
              string a = tb2.Text.Replace("-", "");
              tb2.Text = a;
              string b = tb2.Text.Replace(",", "");
              tb2.Text = b;
              string с = tb2.Text.Replace(".", "");
              tb2.Text = с;
        }
    }
}
