﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shakirov
{
    public partial class агенты : Form
    {
        public агенты()
        {
            InitializeComponent();
            dgv1.Columns[0].Width = 200;
            dgv1.Columns[1].Width = 80;
            dgv1.Columns[3].Width = 180;
            dgv1.Columns[4].Width = 150;
            dgv1.Columns[5].Width = 50;
            l5.Font.Bold.ToString();
            function();
        }
        int a = 0;
        /// <summary>
        /// реализация выделения
        /// </summary>
        private void function()
        {
            try
            {
                // цикл по datagridview1
                for (int i = 0; i < dgv1.Rows.Count; i++)
                {
                    a++;
                    if (dgv1.Rows[i].Cells[6].Value.ToString() == "25" || dgv1.Rows[i].Cells[6].Value.ToString() == "24" ||
                        dgv1.Rows[i].Cells[6].Value.ToString() == "28" || dgv1.Rows[i].Cells[6].Value.ToString() == "29" ||
                        dgv1.Rows[i].Cells[6].Value.ToString() == "26")
                    {
                        dgv1.Rows[i].DefaultCellStyle.BackColor = Color.LightGreen;
                    }
                    else
                    {

                    }
                }


            }
            catch {
                MessageBox.Show("Ошибка");
            }     
            }

        private void агенты_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "user3DataSet.Agent". При необходимости она может быть перемещена или удалена.
            this.agentTableAdapter.Fill(this.user3DataSet.Agent);
            function();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                // сортировка столбцов
                if (cb1.Text == "По возрастанию (Наименование)")
                {
                    this.dgv1.Sort(this.dgv1.Columns[0], ListSortDirection.Ascending);
                }
                if (cb1.Text == "По убыванию (Наименование)")
                {
                    this.dgv1.Sort(this.dgv1.Columns[0], ListSortDirection.Descending);
                }
                if (cb1.Text == "По возрастанию (Приоритет)")
                {
                    this.dgv1.Sort(this.dgv1.Columns[5], ListSortDirection.Ascending);
                }
                if (cb1.Text == "По убыванию (Приоритет)")
                {
                    this.dgv1.Sort(this.dgv1.Columns[5], ListSortDirection.Descending);
                }
                function();
            }
            catch
            {
                MessageBox.Show("Ошибка");
            }
        }

        private void cb2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            
        }
        /// <summary>
        /// реализация поиска агента по email и телефону
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tb1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (cb3.Text == "Email")
                {
                    BindingSource bs = new BindingSource();
                    bs.DataSource = dgv1.DataSource;
                    bs.Filter = "Email like '" + tb1.Text + "%'";
                    dgv1.DataSource = bs;
                }
                if (cb3.Text == "Phone")
                {
                    BindingSource bs = new BindingSource();
                    bs.DataSource = dgv1.DataSource;
                    bs.Filter = "Phone like '" + tb1.Text + "%'";
                    dgv1.DataSource = bs;
                }
                function();
            }
            catch
            {
                MessageBox.Show("Ошибка");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var v1 = new приоритет();
            v1.ShowDialog();
            
        }

        private void b5_Click(object sender, EventArgs e)
        {
            Hide();
            var v1 = new добавление();
            v1.Show();
        }

        private void b4_Click(object sender, EventArgs e)
        {
            
            var v1 = new редактирование();
            v1.ShowDialog();
        }

        private void b7_Click(object sender, EventArgs e)
        {
            Hide();
            var v1 = new авторизация();
            v1.Show();
        }

        private void b6_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        
        private void b1_Click(object sender, EventArgs e)
        {
            if (l5.Visible == false)
            {
                MessageBox.Show("Последняя страница!");
            }
            l5.Visible = false;
            l6.Visible = false;
        }

        private void b2_Click(object sender, EventArgs e)
        {
            if (l5.Visible == true)
            {
                MessageBox.Show("Начальная страница!");
            }
            l5.Visible = true;
            l6.Visible = true;
        }
    }
}
