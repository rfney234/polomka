﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shakirov
{
    public partial class редактирование : Form
    {
        public редактирование()
        {
            InitializeComponent();
        }

        private void редактирование_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "user3DataSet.AgentType". При необходимости она может быть перемещена или удалена.
            this.agentTypeTableAdapter.Fill(this.user3DataSet.AgentType);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "user3DataSet.Agent". При необходимости она может быть перемещена или удалена.
            this.agentTableAdapter.Fill(this.user3DataSet.Agent);

        }

        private void b2_Click(object sender, EventArgs e)
        {
            Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Hide();
            MessageBox.Show("Готово!");
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
           
           
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            
        }

        private void b1_Click(object sender, EventArgs e)
        {
            if (cb3.Text == "" || tb2.Text == "" || tb3.Text == "" || tb4.Text == "" || tb5.Text == "" || tb6.Text == "" || tb7.Text == ""
                   || tb8.Text == "" || tb9.Text == "")
            {
                MessageBox.Show("Заполните все поля!");
            }
            else
            {
                Hide();
                MessageBox.Show("Готово!");
            }
        }
    }
}
