﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shakirov
{
    public partial class авторизация : Form
    {
        public авторизация()
        {
            InitializeComponent();
            //скрытие кода
            tb1.UseSystemPasswordChar = true;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

            if (cb1.Checked == true)
            {
                tb1.UseSystemPasswordChar = false;
            }
            else
            {
                tb1.UseSystemPasswordChar = true;
            }
        }

        private void b2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        /// <summary>
        /// реализация входа админитсратора
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void b1_Click(object sender, EventArgs e)
        {
            if (tb1.Text == "0000")
            {
                Hide();
                var v1 = new агенты();
                v1.Show();
                MessageBox.Show("Добро пожаловать, Администратор!");
            }
            else
            {
                MessageBox.Show("Ошибка, неверный код!");
            }
        }
    }
}
