﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TsygnkovAlen
{
    public partial class Материалы : Form
    {
        public Материалы()
        {
            InitializeComponent();
        }

        private void Материалы_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "user13DataSet.Material". При необходимости она может быть перемещена или удалена.
            this.materialTableAdapter.Fill(this.user13DataSet.Material);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Меню меню = new Меню();
            меню.Show();
            Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Данные будут стерты, все равно продолжить ?");
        }
    }
}
