﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TsygnkovAlen
{
    public partial class Агенты : Form
    {
        public Агенты()
        {
            InitializeComponent();
        }

        private void Агенты_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "user13DataSet.Agent". При необходимости она может быть перемещена или удалена.
            this.agentTableAdapter.Fill(this.user13DataSet.Agent);
            dataGridView1.Rows[4].DefaultCellStyle.BackColor = Color.LightGreen;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Меню меню = new Меню();
            меню.Show();
            Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            добавить добавить = new добавить();
            добавить.Show();
            Hide();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Запись будет удалена, все равно продолжить ?");
        }
    }
}
