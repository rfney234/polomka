﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TsygnkovAlen
{
    public partial class Продукция : Form
    {
        public Продукция()
        {
            InitializeComponent();
        }

        private void Продукция_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "user13DataSet.Product". При необходимости она может быть перемещена или удалена.
            this.productTableAdapter.Fill(this.user13DataSet.Product);

        }

        private void button4_Click(object sender, EventArgs e)
        {
            Меню меню = new Меню();
            меню.Show();
            Hide();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Вся инормация о товаре будет удалена, все равно продолжить ?");
        }
    }
}
